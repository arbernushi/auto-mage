<!DOCTYPE html>
<html lang="en">
<head>
<title>Auto MAGE Alpha 1.0.1 Home</title>

<link rel="stylesheet" href="index\style.css">
<link rel="stylesheet" href="index\bootstrap.min.css">

<script src="index/jquery-3.5.1.min.js"></script>
<script src="index/bootstrap.bundle.min.js"></script>

</head>
<body>
<div class="center-show d-flex align-items-center justify-content-center col-md-12" >
<header class="masthead d-flex align-items-center">
            <div class="container px-4 px-lg-5 align-items-center text-center">
                <h1 class="mb-1">Auto MAGE <i>_ Alpha</i></h1></h1>
                <h3 class="mb-5"><em>Your Workshop, in the Palm of Your Hand</em></h3>
                <a class="btn btn-info btn-lg" href="singnin.php"><h1>Enter</h1></a>
            </div>
        </header>
</div>   
    <footer>
        <div class="row">
            <div class="d-flex align-items-center justify-content-center col-md-12">
                <p>Copyright &copy; 2021 Nushi | 
                <a href="#" class="text-white">Terms</a>  | 
                <a href="#" class="text-white">Policy</a>
                </p>
            </div>
        </div>
    </footer>
</div>

</body>
</html>
